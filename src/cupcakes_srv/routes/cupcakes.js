const express = require('express');
const router = express.Router();
const pool = require('../db/db.js')

router.post('/', (req, res, next) => {
  const values = [ req.body.cupcake.title,
				   req.body.cupcake.body,
				   req.body.cupcake.price
				];
  pool.query(` INSERT INTO cupcakes(title, body, price, date_created)VALUES( $1, $2, $3, NOW() ) ON CONFLICT DO NOTHING `, values, 
  (q_err, q_res) => {
     res.json(q_res.rows);
  });
});

router.get('/', (req, res, next) => {
  pool.query(` SELECT * FROM cupcakes  `, [ ], 
  (q_err, q_res)=>{
    res.json(q_res.rows);
  });
});

module.exports = router;
