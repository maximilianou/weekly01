const { Pool } = require('pg');
const pool = new Pool(
    {
        user: 'postgres',
        host: 'db',
        database: 'cupcakesdb',
        password: 'example',
        port: 5432

    }
);
module.exports = pool;