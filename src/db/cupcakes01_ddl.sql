CREATE DATABASE cupcakesdb;

CREATE TABLE users (
  usr_id SERIAL PRIMARY KEY,
  username VARCHAR(255) UNIQUE,
  email VARCHAR(255),
  email_verified BOOLEAN,
  date_created DATE,
  last_login DATE
);

CREATE TABLE cupcakes (
  cup_id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  body VARCHAR,
  price NUMERIC (10, 2),
  date_created TIMESTAMP
);

CREATE TABLE orders (
  or_id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  or_usr_id INT REFERENCES users(usr_id),
  date_created TIMESTAMP
);

CREATE TABLE orders_items (
  or_it_id SERIAL PRIMARY KEY,
  title VARCHAR(255),
  or_id INT REFERENCES orders(or_id),
  or_cup_id INT REFERENCES cupcakes(cup_id),
  or_it_many INT,
  date_created TIMESTAMP
);
