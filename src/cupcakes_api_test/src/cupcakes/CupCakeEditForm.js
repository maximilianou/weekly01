import React, { useState } from 'react';

const CupCakeEditForm = (props) => {
    const [cake, setCake] = useState(props.currCake);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setCake({ ...cake, [name]: value });
    }
    return (
        
    );
}