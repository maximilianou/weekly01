import React from 'react';

const CupCakeTable = (props) => (
    <table>
        <thead>
            <tr>
                <th>Title</th>
                <th>Body</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            {props.cupCakes.length > 0 ? (
                props.cupCakes.map((cake) => (
                    <tr>
                        <td>{cake.title}</td>
                        <td>{cake.body}</td>
                        <td>{cake.price}</td>
                        <td>
                            <button
                                onClick={() => props.editRow(cake)}
                                className='button muted-button'> Edit </button>
                            <button
                                onClick={() => props.deleteCupCake(cake.id)}
                                className='button muted-button'> Delete </button>
                        </td>
                    </tr>
                ))
            ) : (
                    <tr>
                        <td colSpan={4}>No CupCakes</td>
                    </tr>

                )}

        </tbody>
    </table>
);
export default CupCakeTable;