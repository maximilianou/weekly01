import React, { useState } from 'react';

const CupCakeAddForm = (props) => {

    const initialState = { id: null, title: '', body: '', price: 0 };
    const [cupCake, setCupCake] = useState(initialState);
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setCupCake({ ...cupCake, [name]: value });
    }

    return (
        <form onSubmit={(event) => {
            event.preventDefault();
            if (!cupCake.title || !cupCake.body || !cupCake.price) return;
            props.addCupCake(cupCake);
            setCupCake(initialState);
        }}>
            <label>Title</label>
            <input type='text' name='title' value={cupCake.title} onChange={handleInputChange} />
            <label>Body</label>
            <input type='text' name='body' value={cupCake.body} onChange={handleInputChange} />
            <label>Price</label>
            <input type='number' name='price' value={cupCake.body} onChange={handleInputChange} />
            <button>Add CupCake</button>
        </form>
    );
}
export default CupCakeAddForm;