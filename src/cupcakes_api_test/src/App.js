import React, { useState } from 'react';
import CupCakeTable from './cupcakes/CupCakeTable';
import CupCakeAddForm from './cupcakes/CupCakeAddForm';

const App = () => {

  const initialState = {
    title: '',
    body: '',
    price: 0,
  };

  const [cupCake, setCupCake] = useState(initialState);

  const cupCakesData = [
    {
      id: 1,
      title: 'Basic',
      body: 'Vanilla',
      price: 2,
    },
    {
      id: 2,
      title: 'Filled',
      body: 'Jam',
      price: 4,
    },
    {
      id: 3,
      title: 'Cover',
      body: 'Chocolate',
      price: 8,
    },
  ];

  const [cupCakes, setCupCakes] = useState(cupCakesData);

  const updateCake = (currCake) => {
    setCupCake(
      { title: currCake.title, body: currCake.body, price: currCake.price }
    );
  };

  const addCupCake = (cake) => {
    cake.id = cupCakes.length + 1;
    setCupCakes([...cupCakes, cake]);
  }

  const deleteCupCake = (id) => {
    setCupCakes(cupCakes.filter(cake => cake.id !== id));
  }

  const [editing, setEditing] = setState(false);
  const initialFormState = { id: null, title: '', body: '', price: 0 };
  const [currentCupCake, setCurrentCupCake] = useState(initialFormState);
  const editRow = (cake) => {
    setEditing(true);
    setCurrentCupCake({ id: cake.id, title: cake.title, body: cake.body, price: cake.price });
  }
  const updateCupCake = (id, updatedCake) => {
    setEditing(false);
    setCupCakes(cupCakes.map(cake => (cake.id === id ? updatedCake : cake)));
  }
  return (
    <div className='container'>
      <h1>CRUD app with Hooks</h1>
      <div className='flex-row'>
        <div className='flex-large'>
          <h2>Add CupCake</h2>
          <CupCakeAddForm addCupCake={addCupCake} />
        </div>
        <div className='flex-large'>
          <h2>View CupCakes</h2>
          <CupCakeTable cupCakes={cupCakes} editRow={editRow} deleteCupCake={deleteCupCake} />
        </div>
      </div>
    </div>
  );
}

export default App;
